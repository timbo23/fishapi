<?php

namespace Infrastructure\Services;

use Domain\Fish\Interfaces\IFishMapper;
use Domain\Fish\Interfaces\IFishRepository;
use Domian\Fish\Models\Fish;

class FishService implements IFishService
{
    private IFishRepository $fishRepository;
    private IFishMapper $fishMapper;
    
    public function __construct(IFishRepository $fishRepository, IFishMapper $fishMapper)
    {
        $this->fishRepository = $fishRepository;
        $this->fishMapper = $fishMapper;
    }
    
    function getFishById(int $id): Fish
    {
        $fishDto = $this->fishRepository->getFishById($id);
        
        return $this->fishMapper->mapDtoToDomainModel($fishDto);
    }
}