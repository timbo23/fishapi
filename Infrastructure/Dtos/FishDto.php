<?php

namespace Infrastructure\Dtos;

class FishDto
{
    public int $id;
    public string $fishName;
    public string $fishType;
    public string $fishSize;
    public string $fishColour;
    
    public function __construct(int $id,
        string $name,
        string $type,
        string $size,
        string $colour)
    {
        $this->id = $id;
        $this->fishName = $name;
        $this->fishType = $type;
        $this->fishSize = $size;
        $this->fishColour = $colour;
    }
}
