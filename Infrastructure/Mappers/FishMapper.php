<?php

namespace Infrastructure\Mappers;

use Domain\Fish\Exception\FishMapperException;
use Domain\Fish\Interfaces\IFishMapper;
use Domian\Fish\Models\Fish;
use Infrastructure\Dtos\FishDto;

class FishMapper implements IFishMapper
{
    
    /**
     * @throws FishMapperException
     */
    function mapDtoToDomainModel(FishDto $fishinfo): Fish
    {
        try
        {
            return new Fish($fishinfo->id,
                $fishinfo->fishName,
                $fishinfo->fishType,
                $fishinfo->fishSize,
                $fishinfo->fishColour);
        }
        catch(\Exception $exception)
        {
            throw new FishMapperException($exception);
        }
    }
}