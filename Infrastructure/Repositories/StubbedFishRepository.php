<?php

namespace Infrastructure\Repositories;

use Domain\Fish\Interfaces\IFishRepository;
use Infrastructure\Dtos\FishDto;

class StubbedFishRepository implements IFishRepository
{
    
    function getFishById(int $fishId): FishDto
    {
        return new FishDto($fishId,
            "Test",
            "fighter fish",
            "medium",
            "red");
    }
}