<?php

namespace Domain\Fish\Interfaces;

use Domian\Fish\Models\Fish;
use Infrastructure\Dtos\FishDto;

interface IFishMapper
{
    function mapDtoToDomainModel(FishDto $fishinfo):Fish;
}