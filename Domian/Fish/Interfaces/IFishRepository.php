<?php

namespace Domain\Fish\Interfaces;

use Infrastructure\Dtos\FishDto;

interface IFishRepository
{
    function getFishById(int $fishId):FishDto;
}