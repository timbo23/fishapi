<?php

namespace Domain\Fish\Interfaces;

use Domian\Fish\Models\Fish;

interface IFishService
{
    function getFishById(int $id):Fish;
}