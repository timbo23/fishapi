<?php

namespace Domian\Fish\Models;

use InvalidArgumentException;

class Fish
{
    private string $name;
    private string $type;
    private string $size;
    private string $colour;
    
    public function __construct(
        string $name,
        string $type,
        string $size,
        string $colour)
    {
        $this->setFishName($name);
        $this->setFishType($type);
        $this->setFishSize($size);
        $this->setFishColour($colour);
    }
    
    private function setFishName(string $name) :void
    {
        empty($name) ? throw new InvalidArgumentException("name is empty") : $this->name = $name;
    }
    
    private function setFishType(string $type) :void
    {
        empty($type) ? throw new InvalidArgumentException("type is empty") : $this->type = $type;
    }
    
    private function setFishSize(string $size) :void
    {
        empty($size) ? throw new InvalidArgumentException("size is empty") : $this->size = $size;
    }
    
    private function setFishColour(string $colour) :void
    {
        empty($colour) ? throw new InvalidArgumentException("colour is empty") : $this->colour = $colour;
    }
}