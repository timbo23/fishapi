<?php

namespace Domain\Fish\Exception;

use Throwable;

class FishMapperException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}